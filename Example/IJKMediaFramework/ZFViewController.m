//
//  ZFViewController.m
//  IJKMediaFramework
//
//  Created by 任子丰 on 06/13/2018.
//  Copyright (c) 2018 任子丰. All rights reserved.
//

#import "ZFViewController.h"
#import <IJKMediaFramework/IJKMediaFramework.h>

@interface ZFViewController ()
@property (nonatomic, strong) NSURL *assetURL;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) IJKFFMoviePlayerController *player;

@end

@implementation ZFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.assetURL = [NSURL URLWithString:@"rtmp://live.hkstv.hk.lxdns.com/live/hks"];
    self.assetURL = [NSURL URLWithString:@"https://www.apple.com/105/media/us/iphone-x/2017/01df5b43-28e4-4848-bf20-490c34a926a7/films/feature/iphone-x-feature-tpl-cc-us-20170912_1280x720h.mp4"];

    //IJKplayer属性参数设置
    IJKFFOptions *options = [IJKFFOptions optionsByDefault];
   
    self.player = [[IJKFFMoviePlayerController alloc] initWithContentURL:self.assetURL withOptions:options];
    [self.player prepareToPlay];
    self.player.view.backgroundColor = [UIColor blackColor];
    self.player.shouldAutoplay = YES;
    
    UIView *playerBgView = [UIView new];
    [self.containerView insertSubview:playerBgView atIndex:0];
    playerBgView.frame = self.containerView.bounds;
    playerBgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [playerBgView addSubview:self.player.view];
    self.player.view.frame = playerBgView.bounds;
    self.player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
